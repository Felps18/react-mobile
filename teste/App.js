import React, {Component} from 'react';


/* Import da biblioteca de navegação entre telas */
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';


import {HomeScreen,ProfileScreen,DetailsScreen,CadastroScreen} from './src/screens';

const TabNavigator = createBottomTabNavigator(
    {
        Home: HomeScreen,
        Profile: ProfileScreen,
        Details: DetailsScreen,
        Cadastro: CadastroScreen,
    },
    {
        tabBarOptions:{
            activeTintColor:'tomato',
            inactiveTintColor:'white',   
            style:{
                backgroundColor:'#694fad',
            },
            showLabel:'false',
        },
        defaultNavigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, horizontal, tintColor}) =>{
                const{routeName} = navigation.state;
                let IconComponent = Ionicons;
                let iconName;

                if(routeName === 'Home'){
                    iconName = 'md-home';//icone para android, caso seja Ios, colocar 'ios-home'
                }else if(routeName === 'Profile') {
                    iconName = 'md-person';
                }else if (routeName === 'Details'){
                    iconName ='md-cart';
                }else if(routeName === 'Cadastro'){
                  iconName ='md-reorder';
                }

                return <IconComponent name={iconName} size={30} color={tintColor}/>
            }

        })
    }
)

export default createAppContainer(TabNavigator);