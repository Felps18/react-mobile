import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
    Image,
    TextInput
}from 'react-native';


 export default class ProfileScreen extends Component{


    constructor(props){//criação do construtor para usar o state
        super(props);
        this.state = {text: ''};//incializando o texto sem texto nenhum e gravando em state
        this.state = {senha:''};
    }
    
    render(){ 
        return(
            <View style={{flex:1}}>
                <View style={{justifyContent:'center'}}>
                    <Image 
                        source={require('../assets/image-felipe.png')}
                    />
                    <Text style={{fontSize: 20}}>Digite aqui seu email</Text>
                    <TextInput
                        style={{height: 40, fontSize: 20,margin:20}}
                        placeholder="Digite aqui seu email!"
                        onChangeText={(text) => this.setState({text}) }
                    />
                    <Text style={{fontSize: 20}}>Digite aqui sua senha</Text>
                    <TextInput
                        style={{height: 40, fontSize: 20,margin:20}}
                        placeholder="******"
                        onChangeText={(senha) => this.setState({senha}) }
                    />
                    <Button
                        style={{margin:10}}
                        title ='Fazer login'
                        onPress= {() => this.props.navigation.navigate('Home')}
                    />
                    
                </View>

            </View>
        );
    }
}
