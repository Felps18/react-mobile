import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
}from 'react-native';


export default class CadastroScreen extends Component{

    constructor(props){
        super(props);
        this.state = {senha: ''};          
        this.state = {data: ''};
        this.state = {name: ''};
        this.state = {text: ''}; 
    }

    render(){
  
        return(
          <View style={{flex:1}}>
                <View style={{justifyContent:'center'}}>
                    <Text style={{fontSize: 20}}>Digite aqui seu Nome</Text>
                    <TextInput
                        style={{height: 40, fontSize: 20,margin:20}}
                        placeholder="Digite aqui seu nome!"
                        onChangeText={(name) => this.setState({name}) }
                    />
                    <Text style={{fontSize: 20}}>Digite aqui sua Data de Nascimento</Text>
                    <TextInput
                        style={{height: 40, fontSize: 20,margin:20}}
                        placeholder="Exemplo: 27/03/200"
                        onChangeText={(data) => this.setState({data}) }
                    />
                    <Text style={{fontSize: 20}}>Digite aqui seu email</Text>
                    <TextInput
                        style={{height: 40, fontSize: 20,margin:20}}
                        placeholder="Digite aqui seu email!"
                        onChangeText={(text) => this.setState({text}) }
                    />
                    <Text style={{fontSize: 20}}>Digite aqui sua senha</Text>
                    <TextInput
                        style={{height: 40, fontSize: 20,margin:20}}
                        placeholder="******"
                        onChangeText={(senha) => this.setState({senha}) }
                    />
                    <Button 
                        style={{margin:10}}
                        title = 'Concluir!'
                        onPress= {() => this.props.navigation.navigate('Home')}
                    />
                </View>

            </View>   
        );
    }
}