
import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
}from 'react-native';

export default  class DetailsScreen extends Component{

    render(){
        const title = this.props.navigation.getParam('title','Anônimo');
        const data = this.props.navigation.getParam('data','Não determinado');
        
        return(
            <View style={{flex:1,flexDirection:'column'}}>
                <View style={{justifyContent:'flex-start'}}>
                    <Text style={{fontSize:40,fontWeight:'bold'}}>Meu carrinho</Text>
                    <Text style={{fontSize:20,margin:10}}>Nome: {JSON.stringify(title)}</Text>
                    <Text style={{fontSize:20,margin:10}}>Age: {JSON.stringify(data)}</Text>
                </View>
                <View style={{flex:1,justifyContent:'flex-end'}}>

                </View>    
            </View>
        );
    }
}