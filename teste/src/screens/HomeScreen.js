import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
    ScrollView,
    SectionList,
    StyleSheet
}from 'react-native';

export default  class HomeScreen extends Component{
 
    render(){
        return(
            <View style={{flex:1,justifyContent:'flex-end'}}>
                <ScrollView>
                <View style={styles.container}>
                    
                    <SectionList
                        sections={[
                            {title:'Heróis', data:['Capitão América','Thor','Hulk','Homem-Aranha','Viúva Negra','Gavião Arqueiro','Homem Formiga','Homem de Ferro']},
                            {title:'Vilões', data:['Loki','Thanos','Venom','Duende Verde','Electro']}
                         ]}
                        renderItem={({item}) => <Text style={styles.item}> {item} </Text>}
                        renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                        keyExtractor={(item,index) => index}
                    />
                    
                </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,//ocupar todo espaço na tela
        paddingTop:22,//espaçamento no topo do celular
    },
    item: {
        padding: 10,//espaçamento
        fontSize: 30,//tamanho da letra
        height: 60,//altura de cada item
    },
    sectionHeader: {//estilo para o title da section
         paddingTop: 10,//espaçamento no topo do celular
         paddingLeft: 10,//espaçamento a esquerda
         paddingRight: 10,//espaçamento a direita
         paddingBottom: 2,
         fontSize: 30,//tamanho da letra
         fontWeight: 'bold',//estilo da letra, negrito
         backgroundColor:'steelblue'//cor de fundo
    }
})