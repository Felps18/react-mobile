import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
}from 'react-native';

/* Import da biblioteca de navegação entre telas */
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

/*
Se eu quiser passar parâmetros/informações da tela inicial para a tela de perfil, 
basta eu passar um objeto como segundo parâmetro dentro da nossa função anomina na props 'onPress'.
Esse objeto é um JSON, trabalha em um formato 'nome' e 'valor'
*/
class HomeScreen extends Component{
    render(){
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela Inicial</Text>
                </View>
                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para Tela Perfil'
                        onPress= {() => this.props.navigation.navigate('Profile',{name:'Marcos',age:28})}
                    />
                </View>
            </View>
        );
    }
}


/*
    Criacao da classe da Tela de perfil 
    Tela que vai receber o parâmetro/informação da tela inicial.
    Vamos criar duas variaveis para trazer o objeto com dois valores
    'getParam()' funcao usada para buscar/pegar o parâmetro, podemos usar dois parametros nessa função:
    1-Valor do parâmetro passado em outra tela
    2-Um valor default caso ele não encotre nada

    Como usamos o JSON no 'nome' 'valor' do objeto, na hora de trazer os valores dentro desse objeto e 
    mostrar na tela precisamos usar {JSON.stringify(name)} ou seja ele vai decompor essa variavel 'name' e
    recuperar seu valor, que no nosso será 'Marcos'

    Prop navigationOptions, na barra de navegação irá aparecer 'PERFIL' definido em 'title'.
    Dentro de headerStyle iremos estilizar a barra de navegação
    Dentro de headerTintColor iremos estilizar a seta que aparece na barra de navegação e a cor do texto
    Dentro de headerTitleStyle iremos estilizar a fonte do texto que deixamos em negrito

*/
class ProfileScreen extends Component{
    static navigationOptions ={
        title:'PERFIL',
        headerStyle:{
            backgroundColor: 'orange'

        },
        headerTintColor:'white',
        headerTitleStyle:{
            fontWeight: 'bold'
        }
        
    };

    render(){
        
        const name = this.props.navigation.getParam('name','Anônimo');
        const age = this.props.navigation.getParam('age','Não determinado');
        
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela de Perfil</Text>
                    <Text style={{fontSize:40}}>Tela de Perfil</Text>
                    <Text style={{fontSize:40}}>Nome: {JSON.stringify(name)}</Text>
                    <Text style={{fontSize:40}}>Age: {JSON.stringify(age)}</Text>
                </View>
                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para Tela de Detalhes'
                        onPress= {() => this.props.navigation.navigate('Details')}
                    />
                </View>
            </View>
        );
    }
}

/*
    Criacao da classe da Tela de detalhes  
    Função goBack() volta para a tela anterior
*/
class DetailsScreen extends Component{
    render(){
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela de Detalhes</Text>
                </View>

                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para a tela Inicial'
                        onPress= {() => this.props.navigation.navigate('Home')}
                    />
                </View>

                <View style={{margin:20}}>
                    <Button 
                        title = 'Voltar'
                        onPress= {() => this.props.navigation.goBack()}
                    />
                </View>
                
                <View style={{margin:20}}>
                    <Button 
                        title = 'Topo'
                        onPress= {() => this.props.navigation.popToTop()}
                    />
                </View>
            
            
            </View>
        );
    }
}

const AppNavigator = createStackNavigator(
    {
        Home: {
            screen: HomeScreen
        },
        Profile: {
            screen: ProfileScreen
        },
        Details: {
            screen: DetailsScreen
        }
    },
    {
        initialRouteName: 'Home'
    }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component{
    render(){
        return <AppContainer/>;
    }
} 