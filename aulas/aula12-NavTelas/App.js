import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
}from 'react-native';

/* Import da biblioteca de navegação entre telas */
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';


/*
    Criação da classe da Tela principal
    Para sair da minha tela inicial e ir para a próxima tela preciso incluir um botão
    dentro da prop 'onPress' criamos uma funcao anonima, e no corpo dessa função
    chamamos um prop do navigation para acessar a proxima tela(rota)
*/
class HomeScreen extends Component{
    render(){
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela Inicial</Text>
                </View>
                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para Tela Perfil'
                        onPress= {() => this.props.navigation.navigate('Profile')}
                    />
                </View>
            </View>
        );
    }
}


/*Criacao da classe da Tela de perfil  */
class ProfileScreen extends Component{
    render(){
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela de Perfil</Text>
                </View>
                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para Tela de Detalhes'
                        onPress= {() => this.props.navigation.navigate('Details')}
                    />
                </View>
            </View>
        );
    }
}

/*
    Criacao da classe da Tela de detalhes  
    Função goBack() volta para a tela anterior
    Função popToTop() do react para voltar a tela de topo, ou seja não importa a tela que você está
    ele vai voltar para a tela inicial
*/
class DetailsScreen extends Component{
    render(){
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela de Detalhes</Text>
                </View>

                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para a tela Inicial'
                        onPress= {() => this.props.navigation.navigate('Home')}
                    />
                </View>

                <View style={{margin:20}}>
                    <Button 
                        title = 'Voltar'
                        onPress= {() => this.props.navigation.goBack()}
                    />
                </View>
                
                <View style={{margin:20}}>
                    <Button 
                        title = 'Topo'
                        onPress= {() => this.props.navigation.popToTop()}
                    />
                </View>
            
            
            </View>
        );
    }
}


/* 
    O createStackNavigator é uma função que quando você chama ele devolve um componente react.
    Ele devolve dois parâmetros:
    1-Objeto com a configuração das rotas(seriam as telas);
    2-Objeto com opções.

    Nesse caso criamos uma variavel que recebe o createStackNavigator. Depois criamos um objeto
    com um nome qualquer que recebe uma propriedade e o valor dessa propriedade('screen':seria a sua rota) 
    vai ser o homeScreen pois você vai dizer para essa função qual é o componente que representa sua tela;

    No segundo parâmetros vamos dizer para a biblioteca que comanda a pilha de navegação quais das telas
    criadas é a principal "initialRouteName: 'Home' ", então nossa tela principal é a 'Home'

*/

const AppNavigator = createStackNavigator(
    {
        Home: {
            screen: HomeScreen
        },
        Profile: {
            screen: ProfileScreen
        },
        Details: {
            screen: DetailsScreen
        }
    },
    {
        initialRouteName: 'Home'
    }
);

/*
    O createAppContainer serve para conter seu navegador
    No nosso caso criamos uma variavel recebendo createAppContainer(container principal) e como parâmetro
    passamos o AppNavigator(nosso navegador) nosso objeto;
    Além disso podemos renderizar esse container
*/

const AppContainer = createAppContainer(AppNavigator);

/*
    Criação da classe  principal
    Estamos retornando nosso container principal que tem como o objeto nosso navegador
*/
export default class App extends Component{
    render(){
        return <AppContainer/>;
    }
}