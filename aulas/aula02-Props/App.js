import React, { Component } from 'react';
import {
   Text,
   View,
   Image
   } from 'react-native';

class Ola extends Component{//Criando Um Componente
  render(){
    return(
    /*1.Colocando um atributo style e alinhando o texto no centro
     2.Criando uma  props para passar o parâmetro dentro componente. 
     No caso a propriedade seria um nome(String) */
    <View style={{alignItems:'center'}}>
        <Text>Ola {this.props.name}!</Text>
    </View>
    );
  }
}

export default class Saudacoes extends Component {
  render() {
        return(
          /*fazendo chamada ao componente Ola
           e passando dentro da props('name') o nome que eu quero como parâmetro*/
          <View style={{alignItems:"center"}}>
            <Ola name ='Felipe'/>
            <Ola name ='Marcos'/>
            <Ola name ='Paulo'/>
          </View>
        );
  }
}
