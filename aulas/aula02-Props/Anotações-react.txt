----- Aula 02 -----
Props

1-Usar Props(Properties) é uma maneira de customizar os componentes no React Native;

2-Dentro da tag <Image/> temos uma props chamada "source" onde irá passar um parâmetro podendo ser uma variável que guarda um endereço
de uma imagem ou um endereço da imagem; 

3-Podemos criar um componente e dentro uma propriedade;

4-Todo componente tem que ter a função render pra informar o que será renderizado;

5- Para usar códigos JavaScript precisamos abrir e fechar chaves.Ex:
{this.props.name}

6- A tag <View> seria um container na qual eu posso colocar outros componentes dentro(Text,Image e etc..);