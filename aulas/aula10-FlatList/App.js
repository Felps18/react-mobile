import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    StyleSheet,
    TextInput,
    Button,
    Alert,
    ScrollView,
    FlatList,
} from 'react-native';
/*Novo importe do componente FlatList */

export default class FlatListApp extends Component{
    /*
        'data' props onde irá guardar as informações ou a lista de items para carrregar
        na tela

        No nosso caso estamos acrescentando items dentro da props 'data' usando o 'key'
        deixando assim fixo no código

        'renderItem' props na qual vai pegar cada item da lista e formatar em um componente que
        será renderizado na tela

        No nosso caso iremos renderizar os elementos em texto( pode ser imagem) e faremos isso
        usando uma funcao anonima recebendo um parâmetro e no seu corpo um componente 'Text'
        chamando  o conteúdo daquele item(key) e renderizando em um texto
    */
    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    data={[
                        {Key:'Homem de Ferro'},
                        {Key:'Capitão América'},
                        {Key:'Thor'},
                        {Key:'Hulk'},
                        {Key:'Homem-Aranha'},
                        {Key:'Viúva Negra'},
                        {Key:'Gavião Arqueiro'},
                        {Key:'Homem Formiga'},
                        {Key:'Homem de Ferro'},
                        {Key:'Capitão América'},
                        {Key:'Thor'},
                        {Key:'Hulk'},
                        {Key:'Homem-Aranha'},
                        {Key:'Viúva Negra'},
                        {Key:'Gavião Arqueiro'},
                        {Key:'Homem Formiga'},
                    ]}
                    renderItem={({item}) => <Text style={styles.item}> {item.Key} </Text>}

                />
            </View>
        );
    }
}

//criando estilos para os componentes
const styles = StyleSheet.create({
    container:{
        flex:1,//ocupar todo espaço na tela
        paddingTop:22,//espaçamento no topo do celular
    },
    item: {
        padding: 10,//espaçamento
        fontSize: 30,//tamanho da letra
        height: 60,//altura de cada item
    }
})