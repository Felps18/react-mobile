import React, { Component } from 'react';
import {
   Text,
   View,
   Image
   } from 'react-native';

class Clock extends Component{//criando um componente para hora
  constructor(props){//construtor passando como parametro um props e chamando o props no super para defini-lo
    super(props)
    //criando um state passando como parametro um date
    this.state = {date: new Date()};

  /*
  1-função para deixar o relógio atual sem ser estático
  2-Quando eu quero atualizar o valor de state eu uso o "setState" passando um novo objeto atualizado
  obs:setInterval precisa ficar dentro do constructor pois é o constructor quem vai ser chamado para mostrar
    a hora na tela, e o setInterval é o responsavel por atualizar essa hora.
  */
  setInterval(
  () => {this.setState({date: new Date()})},
  1000//executar a função a cada 1 segundo, ou seja atualizando o valor do objeto 'Date' a cada 1 segundo; 
  );
  
}


  render() {
    return(
      /*Nesse componente text iremos exibir a hora do celular
        chamando o 'state' para trazer o objeto 'Date' e mostrar a hora
      */
      <Text style={{fontSize:50}}>Hora: {this.state.date.toLocaleTimeString()}</Text>//isso é um JSX
    );
  }
}

export default class ClockApp extends Component{//classe principal
  render(){
    return(
      /*Fazendo a chamada ao componente clock e mostrando a hora na tela do celular */
      <View>
        <Clock/>
      </View>
    );

  }
}

