import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    StyleSheet,
    TextInput,
    Button,
    Alert,
    ScrollView,
    FlatList,
    SectionList,
} from 'react-native';
/*Novo importe do componente SectionList */

export default class SectionListApp extends Component{
    /*
        'sections' props de listView
        Como sectionList é uma lista mais elaborada colocamos o nome da sessão e o nome de cada
        item na sessão usando 'title' e dentro de 'data' colocamos os itens da lista

        No nosso caso temos duas seções, de heróis e vilões
 
        'renderSectionHeader' props para renderizar uma seção

        No nosso caso estamos criamos uma função anonima e passamo como parametro a section
        e no corpo da funcao estamos chamado o title da section e renderizando em texto

        'keyExtractor' props que vai percorrer cada item especificado e gerar uma 'key' para
        cada um deles

        No nosso caso criamos uma função anonima, recebendo dois parâmetros: item e o index.
        e no corpo da função retornamos o index que na verdade é uma key que o react native
        vai gerar para cada item na lista ou seja vai organizar os items para mostrar na tela
    */
    render(){
        return(
            <View style={styles.container}>
                <SectionList
                    sections={[
                        {title:'Heróis', data:['Capitão América','Thor','Hulk','Homem-Aranha','Viúva Negra','Gavião Arqueiro','Homem Formiga','Homem de Ferro']},
                        {title:'Vilões', data:['Loki','Thanos','Venom','Duende Verde','Electro']}
                    ]}
                    renderItem={({item}) => <Text style={styles.item}> {item} </Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item,index) => index}
                />
            </View>
        );
    }
}

//criando estilos para os componentes e para as sections 
const styles = StyleSheet.create({
    container:{
        flex:1,//ocupar todo espaço na tela
        paddingTop:22,//espaçamento no topo do celular
    },
    item: {
        padding: 10,//espaçamento
        fontSize: 30,//tamanho da letra
        height: 60,//altura de cada item
    },
    sectionHeader: {//estilo para o title da section
         paddingTop: 10,//espaçamento no topo do celular
         paddingLeft: 10,//espaçamento a esquerda
         paddingRight: 10,//espaçamento a direita
         paddingBottom: 2,
         fontSize: 30,//tamanho da letra
         fontWeight: 'bold',//estilo da letra, negrito
         backgroundColor:'steelblue'//cor de fundo
    }
})