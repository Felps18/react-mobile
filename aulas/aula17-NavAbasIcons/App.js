import React, {Component} from 'react';
import {
    View,
}  from 'react-native';

import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';

/*Importando o componente de ícones do pacote que baixamos no nosso projeto */
import Ionicons from 'react-native-vector-icons/Ionicons';


/*import dos componentes de telas  */
import{HomeScreen,ProfileScreen} from './src/screens';

/*
    Agora vamos usar o componente de ícones
    Tiramos o estilo para labels pois vamos usar icons

    Para não mostar o nome nas abas usamos um atributo 'showLabel' e setamos como false
    
    Usamos uma segunda propriedade 'defaultNavigationOptions' e nela vamos setar o ícone.
    Dentro dessa props criamos uma função anonima passando um parametro, uma prop 'navigation',
    porque  nessa prop podemos acessar objetos state e lá podemos pegar o nome da rota
    e sabendo isso podemos colocar o ícone certo para cada rota

    Dentro da função criada usamos uma propriedade 'tabBarIcon' que vai receber uma função, tendo
    como parâmetros de entrada:
        focused- é um boolean, vai retornar 'true' caso a aba esteja ativa
        horizontal- é um boolean, se ele retornar 'true' é pq o celular está em modo paisagem
        tintColor- é a cor que vai aparecer no nosso ícone.
    Já no corpo da função declaramos três variáveis :
        routeName- Nome da rota, é por ela que vamos saber a tela certa para aplicar o ícone correto.Essa
        informação vamos pegar da prop navigation.state
        IconComponent- componente do ícone que vamos montar, recebendo Ionicons
        IconName- nessa variavel vamos colocar o nome do icone, e esse nome vai variar

        Depois retornamos o componente IconComponent, passando a prop  nome, prop do tamanho
        do ícone e a prop da cor 
*/
const TabNavigator = createBottomTabNavigator(
    {
        Home: HomeScreen,
        Profile: ProfileScreen,
    },
    {
        tabBarOptions:{
            activeTintColor:'tomato',
            inactiveTintColor:'white',   
            style:{
                backgroundColor:'#694fad',
            },
            showLabel:'false',
        },
        defaultNavigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, horizontal, tintColor}) =>{
                const{routeName} = navigation.state;
                let IconComponent = Ionicons;
                let iconName;

                if(routeName === 'Home'){
                    iconName = 'md-home';//icone para android, caso seja Ios, colocar 'ios-home'
                }else if(routeName === 'Profile') {
                    iconName = 'md-person';
                }

                return <IconComponent name={iconName} size={50} color={tintColor}/>
            }

        })
    }
)

export default createAppContainer(TabNavigator);