**** Aula07 ****
Manipulando Entrada de Texto

Componente: <Text input>

1-Função ".split(' ')" separa as palavras no texto e retornar um array;

2-'placeholder'- texto default que vai parecer na caixa de texto;

3-'onChangeText'- A partir do momento que o usuario vai digitando texto podemos guardar 
tudo aquilo que está sendo digitando em alguma variavel, no nosso caso na variavel 'text';

4- Função ".map()" podemos passar uma função de callBack(vai agir sobre todos os elementos
presentes no array);