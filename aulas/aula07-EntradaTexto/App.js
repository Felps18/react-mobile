import React, {Component} from 'react';
import{
    Text,
    View,
    Image,
    StyleSheet,
    TextInput
}from 'react-native';
/*
    Adicionando um componente novo "TextInput" para receber entrada de texto
*/

export default class PizzaTranslatorApp extends Component{

    /*
        Criando um pequeno programinha para transformar cada palavra digitada pela tela
        do celular virar um pedaço de pizza no formato de um gif

        Criou se uma função anonima dentro de 'onChangeText' tendo como parametro um texto 
        e setando na variavel 'text' esse texto

        No final criamos um component 'Text' e fazemos uma chamada ao state para pegar o seu 
        objeto texto e transformar em pizza e mostrar na tela
        
        Criou se uma função anonima dentro de 'map' tendo como parametro uma variavel 
        e no corpo dessa função feito um ternário para ver se essa variavel está preenchida,
        se sim seta a pizza senão seta vazio

    */

    constructor(props){//criação do construtor para usar o state
        super(props);
        this.state = {text: ''};//incializando o texto sem texto nenhum e gravando em state
    }

    
    render(){
        return(
            <View style={{padding: 40}}>
                <TextInput
                    style={{height: 50, fontSize: 25}}
                    placeholder="Digite aqui para traduzir!"
                    onChangeText={(text) => this.setState({text}) }
                />
                <Text style={{padding: 10, fontSize: 30}}>
                    {this.state.text.split(' ').map( (word) => (word) ? '*':'' )}
                </Text>
            </View>
        );
    }
}