import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
    Image,
}from 'react-native';

/*
    Importamos o componente image    
    Nessa tela iremos mostrar a imagem de perfil
*/

export default class ProfileScreen extends Component{
    static navigationOptions ={
        title:'PERFIL',
        headerStyle:{
            backgroundColor: 'orange'

        },
        headerTintColor:'white',
        headerTitleStyle:{
            fontWeight: 'bold'
        }
        
    };
/*
    Importamos o componente image 
    
    Dentro de image há uma prop chamada 'source', e ela para quem você vai dizer aonde está
    a imagem que você quer renderizar na tela.
    
    E dentro de source utilizamos o "required('')" para especificar o caminho do arquivo
    
    Observação: como estamos dentro da pasta 'screen' se colocarmos './' ele irá procurar a imagem dentro
    de 'screen'. Para sair/voltar a pasta que anteceda 'screen' usamos os '../'(dois pontos e barra) e 
    na sequência colocamos o nome da pasta que queremos acessar e o nome da imagem que iremos usar

*/
    render(){
        
        const name = this.props.navigation.getParam('name','Anônimo');
        const age = this.props.navigation.getParam('age','Não determinado');
        
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                
                <View style={{margin:20}}>
                    <Image 
                    source={require('../assets/image-felipe.png')}
                    />
                </View>
                
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela de Perfil</Text>
                    <Text style={{fontSize:40}}>Tela de Perfil</Text>
                    <Text style={{fontSize:40}}>Nome: {JSON.stringify(name)}</Text>
                    <Text style={{fontSize:40}}>Age: {JSON.stringify(age)}</Text>
                </View>
                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para Tela de Detalhes'
                        onPress= {() => this.props.navigation.navigate('Details')}
                    />
                </View>
            </View>
        );
    }
}
