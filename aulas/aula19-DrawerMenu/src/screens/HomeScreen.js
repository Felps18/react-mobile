import React, {Component} from 'react';
import{
    View,
    Text,
    Image,
    Button
}from 'react-native';

/**
 * Como agora o HomeScreen foi para em outra pilha de navegação retiramos o navigationsOptions 
 * daqui e jogamos para a classe principal
 */
export default class HomeScreen extends Component{

    render(){
        return(
            <View style={{flex:1, justifyContent:'center'}}>
                
                <View style={{margin:80}}>
                    <Image source={require('../assets/image-felipe.png')}/>
                </View>

                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:30}}>Tela Inicial</Text>
                </View>

                <View style={{margin:20}}>
                    <Button 
                        onPress={()=> this.props.navigation.navigate('Home2')}
                        title="Tela Inicial 2"
                    />
                </View>
            
            </View>
        );
    }
}