import React, {Component} from 'react';
import{
    View,
    Text,
    Image
}from 'react-native';


export default class SettingsScreen extends Component{

    render(){
        return(
            <View style={{flex:1, justifyContent:'center'}}>
                
                <View style={{margin:80}}>
                    <Image source={require('../assets/image-js.png')}/>
                </View>

                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:30}}>Tela de Configuração</Text>
                </View>
            
            </View>
        );
    }
}