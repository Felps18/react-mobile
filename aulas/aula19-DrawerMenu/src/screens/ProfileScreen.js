import React, {Component} from 'react';
import{
    View,
    Text,
    Image
}from 'react-native';

export default class ProfileScreen extends Component{

    render(){
        return(
            <View style={{flex:1, justifyContent:'center'}}>
                
                <View style={{margin:80}}>
                    <Image source={require('../assets/image-fomedepoder.png')}/>
                </View>

                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:30}}>Tela de Perfil</Text>
                </View>
            
            </View>
        );
    }
}