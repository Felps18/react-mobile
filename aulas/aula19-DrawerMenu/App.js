import React, {Component} from 'react';
import{
    View,
    Text,
    TouchableOpacity,
    Image
}from 'react-native';

/* Import dos componentes de tela */
import{ProfileScreen,HomeScreen,SettingsScreen} from './src/screens';
import{createAppContainer} from 'react-navigation';
import{createStackNavigator} from 'react-navigation-stack';
import{createDrawerNavigator} from 'react-navigation-drawer';
import HomeScreen2 from './src/screens/HomeScreen2';

/**
 * Iremos criar mais uma pilha de navegação por conta do ícone de menu e usaremos
 * o createStackNavigator(aula12)
 * E para fazermos isso criaremos mais uma classe ou seja um componente a mais
 * para gerenciar esse menu
 * 
 * Criamos uma função para dar um start nesse menu
 * e usamos o objeto navigation que gerencia a pilha de navegação
 * 'toggleDrawer'- fazer o menu lateral aparecer com o clique em cima da imagem
 * 
 * 'TouchableOpacity'-Ele trabalha em cima de uma imagem e nessa imagem ele pode deixar sensível
 *  a toque. Então quanto tocarmos na imagem ele pode chamar um função para fazer determinada coisa.
 *  Então podemos usar uma props 'onPress' e chamar a função toggleDrawer e para linkar a função a 
 *  instância do componente usamos o '.bind(this)'
 * 
 * Observação: quando usamos o 'this' estamos referenciando a classe/componente em que
 * ele está dentro.Nesse caso referenciamos o NavigationDrawerStructure. O props seria
 * as propriedades em que esse componente tem acesso
 * 
 */

class NavigationDrawerStructure extends Component{
    toggleDrawer = () =>{
        this.props.navigationProps.toggleDrawer();
    };
    
    //como todo componente, precisamos renderizar e retornar um jsx
    render(){
        return( 
            <View style={{flexDirection:'row'}}>
                <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
                    <Image
                        source ={require('./src/assets/image-menu.png')}
                        style={{width:25,height:25,marginLeft:5}}
                    />
                </TouchableOpacity>
            </View>
            
        );
    }
}

/**
 * Criando uma variavel que vai ser nossa pilha de navegação para a rota home
 * e está recebendo o createStackNavigator
 * 
 * createStackNavigator como parâmetro recebe as rotas, e dentro delas vem as 
 * propriedades. No nosso caso definimos uma rota 'Home' que recebe a tela 'HomeScreen', então
 * quem usar essa rota vai para nessa tela.
 * 
 * O 'navigationOptions' recebe um navigation como parâmetro e no corpo da função temos
 * o Título(Vai aparecer na barra de navegação ou seja o Header), o 'headerLeft' vai aparecer 
 * a esquerda do nosso título ou seja o nosso ícone de menu. Então passamos o componente que criamos
 * que vai ser renderizado na tela, e como lá definimos um parâmetro temos que passá-lo aqui também, vai
 * ser o nosso 'navigationProps' pasando o 'navigation'(toggleDrawer faz parte desse objeto)
 * 
 * 'headerStyle' estilizar nossa barra de navegação
 * 
 * Criamos uma rota 'Home2' que vai ser chamada através de um botão dentro de 'HomeScreen' que está na 
 * rota 'Home', agora a pilha de navegação 'HomeStackNavigator' possui duas telas. Ou seja 'HomeScreen' passa
 * a enxergar a 'HomeScreen2'
 */
const HomeStackNavigator = createStackNavigator({
    Home:{
        screen: HomeScreen,
        navigationOptions:({navigation}) =>({
            title:'Tela Inicial',
            headerLeft: <NavigationDrawerStructure navigationProps={navigation}/>,
            headerStyle:{
                backgroundColor:'blue',
            },
            headerTintColor:'white',
        })
    },
    
    Home2:{
        screen: HomeScreen2,

    }
})

const ProfileStackNavigator = createStackNavigator({
    Home:{
        screen: ProfileScreen,
        navigationOptions:({navigation}) =>({
            title:'Tela Perfil',
            headerLeft: <NavigationDrawerStructure navigationProps={navigation}/>,
            headerStyle:{
                backgroundColor:'blue',
            },
            headerTintColor:'white',
        })
    },
})

const SettingsStackNavigator = createStackNavigator({
    Home:{
        screen: SettingsScreen,
        navigationOptions:({navigation}) =>({
            title:'Tela Configuração',
            headerLeft: <NavigationDrawerStructure navigationProps={navigation}/>,
            headerStyle:{
                backgroundColor:'blue',
            },
            headerTintColor:'white',
        })
    },
})
/**
 * Aqui invés de chamarmos a tela diretamente pela rota vamos passar a pilha de navegação
 * 
 * Trocamos o nome da rota para 'HomeMenu' e passamos a tela
 * 
 * O que estava definido nas telas pelo navigationOption agora vai ser definido aqui pois
 * mudou a pilha de navegação, cada tela é gerenciada por uma pilha de navegação
 */
const MyDrawerNavigation  = createDrawerNavigator(
    {
        HomeMenu: {
           screen: HomeStackNavigator,
           navigationOptions:{
            drawerLabel:'Home',
            drawerIcon:({focused, tintColor}) =>(
                <Image 
                    source={require('./src/assets/image-felipe.png')}
                    style={{width:30, height:30}}
                />
            ),
          },
        },
        HomeProfile: { 
            screen: ProfileStackNavigator,
            navigationOptions:{
                drawerLabel:'Profile',
                drawerIcon:({focused, tintColor}) =>(
                    <Image 
                        source={require('./src/assets/image-fomedepoder.png')}
                        style={{width:30,height:30}}
                    />
                ),
            },
            
        },
        HomeSettings: { 
            screen: SettingsStackNavigator,
            navigationOptions:{
                drawerLabel:'Settings',
                drawerIcon:({focused, tintColor}) =>(
                    <Image 
                        source={require('./src/assets/image-js.png')}
                        style={{width:30,height:30}}
                    />
                ),
            },
        },
    },
    {
        contentOptions:{
            activeTintColor:'red',
            labelStyle:{
                fontSize:20,

            }
        }
    }
)
/*
    Passando como parâmetro o MyDrawerNavigation que contem a pilha de navegação 
    para dentro do meu createAppContainer
 */
export default createAppContainer(MyDrawerNavigation);