import React, { Component } from 'react';
import {
   Text,
   View,
   Image,
   StyleSheet
   } from 'react-native';


export default class FlexboxApp extends Component{//classe principal
  render(){
    /*
      Usando propriedade flexDirection(row, column)
      
      Nesse caso eu estou dizendo que os meus componentes filhos vão ser dispostos na tela
      de maneira horizontal(linha) ou seja meu eixo principal é linha.

      Caso queira mudar a direção dos componentes filhos para coluna bastar colocar "column"
    
    */
    return(
      <View style={{flex:1,flexDirection:"row"}}>
        <View style={{width:80, height:80, backgroundColor:'powderblue'}}></View>
        <View style={{width:80, height:80, backgroundColor:'skyblue'}}></View>
        <View style={{width:80, height:80, backgroundColor:'steelblue'}}></View>
      </View>
    );
  }
}
