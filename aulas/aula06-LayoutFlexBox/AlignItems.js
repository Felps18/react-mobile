import React, { Component } from 'react';
import {
   Text,
   View,
   Image,
   StyleSheet
   } from 'react-native';


export default class FlexboxApp extends Component{//classe principal
  render(){
     /*
      Usando propriedade alignItems para alinhar os componentes filhos no eixo secundário
      - flex-start
      - center
      - flex-end
      - stretch(esticar os componentes)

      obs:o alignItems não funciona se o componente filho tiver dimensões fixas, ou seja
      o react native vai usar a dimensão fixa. Sendo assim no primeiro componente View ele
      não vai respeitar o alignItems, já nos demais ele vai usar o alignItems pois o eixo
      secundário é a linha e não há nenhuma dimensão fixa(width) nessa direção(horizontal)
    */
    return(
      <View style={{flex:1,flexDirection:"column",justifyContent:"center", alignItems:"stretch"}}>
        <View style={{width:80, height:80, backgroundColor:'powderblue'}}></View>
        <View style={{height:80, backgroundColor:'skyblue'}}></View>
        <View style={{height:80, backgroundColor:'steelblue'}}></View>
      </View>
    );
  }
}
