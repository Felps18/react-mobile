import React, { Component } from 'react';
import {
   Text,
   View,
   Image,
   StyleSheet
   } from 'react-native';


export default class FlexboxApp extends Component{//classe principal
  render(){
    /*
      Usando propriedade justifyContent para distribuir os componentes filhos
      - flex-start(inicio)
      - center(centro)
      - flex-end(final)
      - space-around(espaço ao redor dos componentes)
      - space-between(espaço entre os componentes)
      - space-evenly(espaço uniforme entre os componentes)  
          
    */
    return(
      <View style={{flex:1,flexDirection:"row", justifyContent:"space-between"}}>
        <View style={{width:80, height:80, backgroundColor:'powderblue'}}></View>
        <View style={{width:80, height:80, backgroundColor:'skyblue'}}></View>
        <View style={{width:80, height:80, backgroundColor:'steelblue'}}></View>
      </View>
    );
  }
}
