import React, {Component} from 'react';
import {
    View,
    Text
} from 'react-native';

/*
    Aba ProfileScreen ou componente ProfileScreen, lembrando que uma tela(screen) é um componente
    
    E para nós exportamos esse componente a classe principal devemos usar o 'export default'
*/
export default class ProfileScreen extends Component{
    render(){
        return(
           <View style={{flex:1, justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:40}}>Tela de Perfil</Text>
                </View>
           </View> 
        );
    }
}