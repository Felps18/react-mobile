import React, {Component} from 'react';
import {
    View,
    Text
} from 'react-native';

/*
    Aba HomeScreen ou componente HomeScreen, lembrando que uma tela(screen) é um componente
    
    E para nós exportamos esse componente a classe principal devemos usar o 'export default'
*/
export default class HomeScreen extends Component{
    render(){
        return(
           <View style={{flex:1, justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:40}}>Tela Inicial</Text>
                </View>
           </View> 
        );
    }
}