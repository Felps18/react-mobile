import React, {Component} from 'react';
import {
    View,
}  from 'react-native';

/* 
    --- Import da biblioteca de navegação entre telas ---

    Utilizamos um novo componente chamado createBottomTabNavigator que seria da pilha
    de navegação entre abas na párte inferior da tela e importamos esse 
    component da biblioteca 'react-navigation-tabs'

    O createAppContainer é o que o react-native vai usar para montar nossa aplicação
*/
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';

/*import dos componentes de telas  */
import{HomeScreen,ProfileScreen} from './src/screens';

/*
    Navegação entre Abas 

    Criar duas abas:HomeScreen e ProfileScreen
   
    Criamos uma variavel TabNavigator para guardar nossa pilha de navegação, ou seja vai ter
    a função createBottomTabNavigator() que recebe dois parâmetros:
   
    O primeiro vai ser um objeto JavaScript que vai conter as rotas da pilha de navegação.As
    rotas seriam as telas ou as abas.Ou seja é nesse parâmetro que vamos dizer quais componentes de
    tela fazem parte da pilha de navegação.
    Fazemos uma jogada de 'key:Value' na qual nossa key é o nome qualquer que damos a aba e vai aparecer no
    nosso aplicativo e o 'value' é o nosso componente tela que criamos

    O sergundo parâmetro e para configurar a aparência das abas na parte inferior da tela
    Criamos um objeto tabBarOptions e dentro dele um labelStyle que seria o estilo da fonte nas abas
    o activeTintColor é uma props que define qual é a cor que fica a letra enquanto a aba  está ativa
    e a inactiveTintColor a cor da letra para quando a aba está inativa
    Podemos definir uma cor de fundo usando a props 'style' e dentro dela passando um backgroundColor
    
*/
const TabNavigator = createBottomTabNavigator(
    {
        Home: HomeScreen,
        Profile: ProfileScreen,
    },
    {
        tabBarOptions:{
            labelStyle:{
                fontSize:35,
            },
            activeTintColor:'tomato',
            inactiveTintColor:'white',   
            style:{
                backgroundColor:'blue'
            }
        }
    }
)
/*
    O createAppContainer vai criar o componente principal da nossa aplicação
    E vai englobar o TabNavigator que é a pilha de navegação composta por duas abas
    
*/
export default createAppContainer(TabNavigator);