import React, { Component } from 'react';
import{
    Text,
    View,
    Image,
    StyleSheet,
    TextInput,
    Button,
    Alert,
    ScrollView
}from  'react-native';
/*Importando novo Componente ScrollView*/

export default class ScrollViewApp extends Component{
    render(){
        return(
            <ScrollView>

                <View style={{
                    margin:20,
                    flexDirection:'row',//mensagem em linha
                    justifyContent:'flex-end',//mensagem do lado direito no final da tela
                }}>
                    <Text style={{fontSize:30}}>Olá tudo bem?</Text>
                </View>

                <View style={{
                    margin:20,
                    flexDirection:'row',//mensagem em linha
                    justifyContent:'flex-start',//mensagem do lado esquerdo da tela
                }}>
                    <Text style={{fontSize:30}}>tudo bem e você?</Text>
                </View>

                <View style={{
                    margin:20,
                    flexDirection:'row',//mensagem em linha
                    justifyContent:'flex-end',//mensagem do lado direito no final da tela
                }}>
                    <Text style={{fontSize:30}}>Olá tudo bem?</Text>
                </View>

                <View style={{
                    margin:20,
                    flexDirection:'row',//mensagem em linha
                    justifyContent:'flex-start',//mensagem do lado esquerdo da tela
                }}>
                    <Text style={{fontSize:30}}>tudo bem e você?</Text>
                </View>
                
                <View style={{
                    margin:20,
                    flexDirection:'row',//mensagem em linha
                    justifyContent:'flex-end',//mensagem do lado direito no final da tela
                }}>
                    <Text style={{fontSize:30}}>Olá tudo bem?</Text>
                </View>

                <View style={{
                    margin:20,
                    flexDirection:'row',//mensagem em linha
                    justifyContent:'flex-start',//mensagem do lado esquerdo da tela
                }}>
                    <Text style={{fontSize:30}}>tudo bem e você?</Text>
                </View>

                <View style={{
                    margin:20,
                    flexDirection:'row',//mensagem em linha
                    justifyContent:'flex-end',//mensagem do lado direito no final da tela
                }}>
                    <Text style={{fontSize:30}}>Olá tudo bem?</Text>
                </View>

                <View style={{
                    margin:20,
                    flexDirection:'row',//mensagem em linha
                    justifyContent:'flex-start',//mensagem do lado esquerdo da tela
                }}>
                    <Text style={{fontSize:30}}>tudo bem e você?</Text>
                </View>
                              
            </ScrollView>
        );
    }
}