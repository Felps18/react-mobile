import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    StyleSheet,
    TextInput,
    Button,
    Alert
}from 'react-native';

/* Novo componente Button e Alert*/

export default class ButtonApp extends Component{//classe principal
    /*Criando um função nova para usar em todos os botões*/
    onClickButton(){
        Alert.alert('Você clicou no botão!');
    }
    
    /*

    'onClickButton' função criada para mostrar um alert e chamada dentro dentro da props 'onPress'

    */
    render(){
        return(
            <View style={styles.container}>
            
                <View style={styles.buttonContainer}>
                    <Button
                    onPress={this.onClickButton}
                    title="Clique aqui"
                    />
                </View>
                <View style={styles.buttonContainer}>
                    <Button                
                    onPress={this.onClickButton}
                    title="Clique aqui"
                    color='green'
                    />
                </View>
                <View style={styles.alternativeButtonContainer}>
                    <Button                
                    onPress={this.onClickButton}
                    title="Parece OK!"
                    color='red'
                    />
                    <Button                
                    onPress={this.onClickButton}
                    title="Tudo certo aqui"
                    color='blue'
                    />
                </View>
            
            </View>
        );
    }
}
// criando variaveis de estilo dentro da folha de estilo 'styles' 
const styles = StyleSheet.create({//estilo para o container principal
    container:{
        flex:1,//vai ocupar toda a tela
        justifyContent:'center',//os componentes filhos vão ser alinhaos ao centro da tela
    },
    buttonContainer:{//estilo para os botões
        margin:20
    },
    alternativeButtonContainer:{//estilo para os dois ultimos botões
        margin:20,
        flexDirection:'row',//os botões vão ser dispostos na linha(horizontal)
        justifyContent: 'space-between'//espaço entre os botões
    }
})