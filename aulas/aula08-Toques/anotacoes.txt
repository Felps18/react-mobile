**** Aula08 ****
Manipulando Toques

Detectar gestos e toques na tela do celular

1-'title' props do componente Button para inserir um texto no botão;

2-'OnPress' props do componente Button, passa uma função dentro dela quando o usário
clica no botão, ou seja essa função realiza uma ação quando apertado;

3-No nosso caso mostramos um alert com um texto quando o usuario clica no botão;

4-React native não suporta varias 'Views' sendo retornadas ao mesmo tempo, então criamos uma 'View'
principal e colocamos todas as outras 'Views' dentro,logo retornamos uma só 'View';

5-'color' props do componente 'Button' usado para mudar a cor do botão;