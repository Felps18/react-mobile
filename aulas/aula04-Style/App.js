import React, { Component } from 'react';
import {
   Text,
   View,
   Image,
   StyleSheet
   } from 'react-native';

  
   /*Definindo estilos para usar no aplicativo com o componente 'StyleSheet'
  Lembrando que nós precisamos importar esse componente para não dar exception
  */
  const styles = StyleSheet.create({//criando uma variavel 'styles' com varios estilos
    bigblue:{//criando um estilo chamado 'bigblue'
      color: 'blue', //cor do texto
      fontWeight: 'bold',//texto em negrito
      fontSize:50,//tamanho da fonte
    },
    red:{//criando outro estilo chamado 'red'
      color: 'red',
      fontSize:30,

    }

  })

export default class StylesApp extends Component{//classe principal
  render(){
    /*Fazendo a chamada aos estilos que foram criados 
      Ultimos componentes text sendo usando um array de estilos com precedência
    */
    return(
      <View>
        <Text style={styles.red}>vermelho</Text>
        <Text style={styles.bigblue}>azul</Text>
        <Text style={[styles.bigblue, styles.red]}>azul, depois vermelho</Text>
        <Text style={[styles.red, styles.bigblue]}>vermelho, depois azul</Text>
      </View>
    );
  }
  
}

