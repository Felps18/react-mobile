import React, {Component} from 'react';
import{
    View,
    Text,
    Image,
    Button
}from 'react-native';

/*
    Componente de tela permite usar o navigationOptions para editar o menu
    drawerLabel serve para gente colocar o nome no item do menu

    Todo componente precisa de um render  pois o react native precisa saber como vai
    montar esse componente na tela do aplicativo

    drawerIcon-podemos definir uma função, nessa função vai receber dois parâmetros:
    focused-é um boolean, se o item do menu está selecionado retorna true
    tintColor- está definindo a cor do item
    No corpo do função vamos definir o component image que será o ícone

    Podemos também usar dois menus, criamos um component Button e passamos dentro de OnPress usamos
    uma prop navigation que tem como parâmetro a rota para onde você quer redirecionar o usuário
*/
export default class HomeScreen extends Component{
    static navigationOptions={
        drawerLabel:'Home',
        drawerIcon:({focused, tintColor}) =>(
            <Image 
                source={require('../assets/image-felipe.png')}
                style={{width:30, height:30}}
            />
        ),
    }

    render(){
        return(
            <View style={{flex:1, justifyContent:'center'}}>
                
                <View style={{margin:80}}>
                    <Image source={require('../assets/image-felipe.png')}/>
                </View>

                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:30}}>Tela Inicial</Text>
                </View>

                <View style={{margin:20}}>
                    <Button 
                        onPress={()=> this.props.navigation.navigate('Profile')}
                        title="Profile"
                    />
                </View>
            
            </View>
        );
    }
}