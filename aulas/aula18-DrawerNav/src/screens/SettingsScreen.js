import React, {Component} from 'react';
import{
    View,
    Text,
    Image
}from 'react-native';

/*
    Componente de tela permite usar o navigationOptions para editar o menu
    drawerLabel serve para gente colocar o nome no item do menu

    Todo componente precisa de um render  pois o react native precisa saber como vai
    montar esse componente na tela do aplicativo
    
    drawerIcon-podemos definir uma função, nessa função vai receber dois parâmetros:
    focused-é um boolean, se o item do menu está selecionado retorna true
    tintColor- está definindo a cor do item
    No corpo do função vamos definir o component image que será o ícone
*/
export default class SettingsScreen extends Component{
    static navigationOptions={
        drawerLabel:'Settings',
        drawerIcon:({focused, tintColor}) =>(
            <Image 
                source={require('../assets/image-js.png')}
                style={{width:30,height:30}}
            />
        ),
    }

    render(){
        return(
            <View style={{flex:1, justifyContent:'center'}}>
                
                <View style={{margin:80}}>
                    <Image source={require('../assets/image-js.png')}/>
                </View>

                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:30}}>Tela de Configuração</Text>
                </View>
            
            </View>
        );
    }
}