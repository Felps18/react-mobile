import React, {Component} from 'react';
import{
    View,
    Text
}from 'react-native';



/* Import dos componentes de tela */
import{ProfileScreen,HomeScreen,SettingsScreen} from './src/screens';
import{createAppContainer} from 'react-navigation';
import{createDrawerNavigator} from 'react-navigation-drawer';

/**
 *  CreateAppContainer, que é o container que vai encapsular toda pilha do meu aplicativo
 *  createDrawerNavigator, para criar a pilha de navegação
 *  
 *  Criamos Uma variavel que recebe a função createDrawerNavigator, essa função recebe um parâmetro
 *  e nesse parâmetro eu digo quais as telas ou pilha de navegação eu quero que ele gerencie
 *  
 *  O createDrawerNavigator ainda pode receber um segundo parâmetro,nele podemos definir as
 *  configurações do menu.
 *  Para mudarmos a cor do item no menu de quando um tela está ativa podemos usar um objeto
 *  chamado contentOptions e nele definimos algumas propriedades:
 *  activeTintColor - quando o item de menu estiver selecionado ele vai ficar da cor que definimos
 *  labelStyle - para configurar a label ou  o texto
 * 
 *  Para usarmos os ícones ao lado de cada label(item) temos que fazer isso em cada tela
 */

const MyDrawerNavigation  = createDrawerNavigator(
    {
        Home: HomeScreen,
        Profile: ProfileScreen,
        Settings: SettingsScreen,
    },
    {
        contentOptions:{
            activeTintColor:'red',
            labelStyle:{
                fontSize:20,

            }
        }
    }
)
/*
    Passando como parâmetro o MyDrawerNavigation que contem a pilha de navegação 
    para dentro do meu createAppContainer
 */
export default createAppContainer(MyDrawerNavigation);