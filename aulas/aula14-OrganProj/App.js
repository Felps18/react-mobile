import React, {Component} from 'react';

/* Import da biblioteca de navegação entre telas */
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

/*
    Imports das classes, precisamos colocar o ponto e barra no começo
    Exemplo: './src'  
    
    Podemos melhorar a questão dos imports sem que se repitam muito, que nosso caso
    tivemos que importar três vezes. 

    1-Basta criar um arquivo 'index.js', nesse arquivo podemos importar os nossos
    três componentes de tela
    
    2-Depois da configuração no arquivo 'index.js' fazemos o import das telas 
    em uma linha só, acessando só a pasta '/screens' sem especificar os nomes
    dos arquivos

    Desse modo utilizamos boas práticas, além de deixar o projeto mais fácil
    para dar manutenção
*/
import {HomeScreen,ProfileScreen,DetailsScreen} from './src/screens'
/*
    Os imports repetidos
    
    import HomeScreen from './src/components/HomeScreen';
    import ProfileScreen from './src/components/ProfileScreen';
    import DetailsScreen from './src/components/DetailsScreen';
*/




/* Com a criação de um arquivo para cada classe(nossas telas) deixamos  o 'App.js' com
    o código mais limpo.
    
    Agora precisamos importar essas classes que estão em aqruivos separados
    Observação: só podemos fazer o import se na classe que queremos importar esteja
    declarado 'export default'
*/

const AppNavigator = createStackNavigator(
    {
        Home: {
            screen: HomeScreen
        },
        Profile: {
            screen: ProfileScreen
        },
        Details: {
            screen: DetailsScreen
        }
    },
    {
        initialRouteName: 'Home'
    }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component{
    render(){
        return <AppContainer/>;
    }
} 