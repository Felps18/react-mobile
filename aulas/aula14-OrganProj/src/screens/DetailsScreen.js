import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
}from 'react-native';

/* Criamos um arquivo dentro da pasta '/screens' somente para nossa tela de Detalhes
    Desse modo temos que importar os componentes que essa classe está usando
    
    Para que nosso arquivo principal(App.js) importe e use essa classe precisamos
    colocar o 'export default'(permitindo fazer exportação p/ outra classe) 
*/


export default class DetailsScreen extends Component{
    render(){
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela de Detalhes</Text>
                </View>

                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para a tela Inicial'
                        onPress= {() => this.props.navigation.navigate('Home')}
                    />
                </View>

                <View style={{margin:20}}>
                    <Button 
                        title = 'Voltar'
                        onPress= {() => this.props.navigation.goBack()}
                    />
                </View>
                
                <View style={{margin:20}}>
                    <Button 
                        title = 'Topo'
                        onPress= {() => this.props.navigation.popToTop()}
                    />
                </View>
            
            
            </View>
        );
    }
}