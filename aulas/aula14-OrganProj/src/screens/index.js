import HomeScreen from './HomeScreen';
import ProfileScreen from './ProfileScreen';
import DetailsScreen from './DetailsScreen';

/*
    Criamos e usamos essa classe somente para realizar os importes dos 
    nossos componentes de tela e depois fazemos o import de um jeito só ou seja
    agrupando e exportando os três componentes em um export.

*/

export{HomeScreen,ProfileScreen,DetailsScreen};