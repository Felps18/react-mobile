import React, {Component} from 'react';
import {
    View,
    Text,
    Button,
}from 'react-native';

/* Criamos um arquivo dentro da pasta '/screens' somente para nossa tela de perfil
    Desse modo temos que importar os componentes que essa classe está usando
    
    Para que nosso arquivo principal(App.js) importe e use essa classe precisamos
    colocar o 'export default'(permitindo fazer exportação p/ outra classe) 
*/

export default class ProfileScreen extends Component{
    static navigationOptions ={
        title:'PERFIL',
        headerStyle:{
            backgroundColor: 'orange'

        },
        headerTintColor:'white',
        headerTitleStyle:{
            fontWeight: 'bold'
        }
        
    };

    render(){
        
        const name = this.props.navigation.getParam('name','Anônimo');
        const age = this.props.navigation.getParam('age','Não determinado');
        
        return(
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:50}}>Tela de Perfil</Text>
                    <Text style={{fontSize:40}}>Tela de Perfil</Text>
                    <Text style={{fontSize:40}}>Nome: {JSON.stringify(name)}</Text>
                    <Text style={{fontSize:40}}>Age: {JSON.stringify(age)}</Text>
                </View>
                <View style={{margin:20}}>
                    <Button 
                        title = 'Ir para Tela de Detalhes'
                        onPress= {() => this.props.navigation.navigate('Details')}
                    />
                </View>
            </View>
        );
    }
}
