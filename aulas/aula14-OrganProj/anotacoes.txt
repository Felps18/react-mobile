**** Aula14 ****
Organizando Projeto

1-React-Native é uma biblioteca, porém ele não define como você tem que organizar seu 
projeto;

2-Estruturar/Modularizar o projeto é uma forma de organizar o seu projeto de modo ficar mais 
facil identificar classes ou arquivos dentro dele;

3-Para Estruturarum projeto podemos criar pastas e subpastas de modo a separar os arquivos que 
possuem classes/funções parecidas;

4- /src seria a pasta "source" na qual geralmente vai o codigo fonte do projeto;

5-Dentro da pasta /src podemos colocar o /assets, um arquivo na qual seriam os ativos ou os 
arquivos que eu uso no meu aplicativo. Exemplo: Imagens que são utilizadas na tela do aplicativo;

6-Dentro da pasta /src podemos colocar o /components, que seriam basicamente os
componentes de botao,form.list,view etc. No entando seria o ideal você colocar nessa pastaos componentes
que você criou, com formato e estilo personalizados;

7-Dentro da pasta /src podemos colocar o /screens, nessa pasta podemos colocar nossos componentes
de tela ou seja as telas que você criou no seu aplicativo;

8-Dentro da pasta /src podemos colocar o /syles, nessa pasta podemos colocar os arquivos com estilo que
aplicamos no nosso aplicativo. Então como uma opção pode se criar um arquivo somente de estilo e 
guardar dentro da pasta /styles;

**************************
Estrutura Basica de pastas
**************************
/src    
    /assets 
    /components
    /screens
    /styles
    
**************************