import React, { Component } from 'react';
import {
   Text,
   View,
   Image,
   StyleSheet
   } from 'react-native';

/*Nessa classe o dimensionamento do componente será dinamico */
export default class FlexDimensionsApp extends Component{//classe principal
  render(){
    
    /* Componente pai usando flex igual a 1, todos os componentes filhos terão que usar
      o tamanho disponivel na tela e compartilhar esse espaço uniformemente

      O segundo componente filho irá ocupar o dobro de espaço na tela do que o primeiro
      O terceiro componente filho irá ocupar o triplo de espaço na tela do que o primeiro
      
    */

    return(
      <View style={{flex:1}}>
        <View style={{flex:1, backgroundColor:'powderblue'}}></View>
        <View style={{flex:2, backgroundColor:'skyblue'}}></View>
        <View style={{flex:3, backgroundColor:'steelblue'}}></View>
      </View>
    );
  }
}
